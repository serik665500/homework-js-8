const parts = document.querySelectorAll("p")
parts.forEach(item => {
    item.style.backgroundColor = ("#ff0000")
})

const opList = document.getElementById("optionsList");
console.log(opList);
console.log(opList.parentElement);
const chilNodes = opList.childNodes;
chilNodes.forEach(nodeItem => {
    console.log('Name of node', nodeItem.nodeName);
    console.log('Type of Node', nodeItem.nodeType);
});

const parContent = document.getElementById('testParagraph');
parContent.textContent = 'This is a paragraph';
console.log(parContent);

const mainHead = document.querySelector('.main-header');

const childElem = mainHead.querySelectorAll('*');

childElem.forEach(elem => {
    elem.classList.add('nav-item');
});
console.log(childElem);

const secTitle = document.querySelectorAll('.section-title');

secTitle.forEach(delElem => {
    delElem.classList.remove('section-title');
})
console.log(secTitle);











